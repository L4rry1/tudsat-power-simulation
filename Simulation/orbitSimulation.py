import constant
import telnetlib
import numpy
import math

HOST = "horizons.jpl.nasa.gov"
PORT = 6775
print("Establishing telnet connection to " + HOST + " via port " + str(PORT) + "...")
try:
    tn = telnetlib.Telnet(HOST, PORT)
    print("Connected!")
except:
    print("Connection failed!")
tn.write("?".encode('ascii') + b"\r")
print(tn.read_some().decode('ascii'))
tn.close()
print(constant.RADIUS_EARTH)

